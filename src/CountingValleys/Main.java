package CountingValleys;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int steps = 10;
		String paths = "UDUUUDUDDD";
		System.out.println(countingValleys(steps, paths));
		
	}
	
	public static int countingValleys(int steps,String path) {
		int alt = 0;
		int marker = 0;
		int counter = 0;
		for(int i = 0;i<path.length();) {
			if(alt == 0) {
				int j = marker;
				if(j == path.length()-1) {
					break;
				}
				if(path.charAt(j) == 'D') {
					alt--;
					counter++;
				}
				else if (path.charAt(j) == 'U'){
					alt++;
				}
				while(true) {
					j++;
					if(path.charAt(j) == 'D') {
						alt--;
					}
					else if(path.charAt(j) == 'U'){
						alt++;
					}
					if( (alt == 0) || (j == (path.length()-1))) {
						break;
					}
					
				}
				marker = j + 1;
				i = marker;
				if(i == (path.length()-1)) {
					break;
				}
				
			}
		}
		return counter;
	}

}
